from flask import Blueprint
from contactosController import ContactosController
from contactosMySQLController import ContactosMySQLController
from environment import Environment

contactosController = ContactosController()
contactosMySQLController = ContactosMySQLController()
bP = Blueprint('bP', __name__)

@bP.route('/contactos/list')
def listarContactos():
    env = Environment()
    bdd = env.swBDD()
    if bdd['BDD']:
        return contactosMySQLController.listar()
    else:
        return contactosController.listar()

@bP.route('/contacto', methods=['POST'])
def anadirContacto():
    env = Environment()
    bdd = env.swBDD()
    if bdd['BDD']:
        return contactosMySQLController.anadir()
    else:
        return contactosController.anadir()


@bP.route('/contacto/<id>', methods=['GET'])
def mostrarContacto(id):
    env = Environment()
    bdd = env.swBDD()
    if bdd['BDD']:
        return contactosMySQLController.mostrar(id)
    else:
        return contactosController.mostrar(id)


@bP.route('/contacto/<id>', methods=['DELETE'])
def eliminarContacto(id):
    env = Environment()
    bdd = env.swBDD()
    if bdd['BDD']:
        return contactosMySQLController.eliminar(id)
    else:
        return contactosController.eliminar(id)


@bP.route('/contacto/<id>', methods=['PUT'])
def actualizarContacto(id):
    env = Environment()
    bdd = env.swBDD()
    if bdd['BDD']:
        return contactosMySQLController.actualizar(id)
    else:
        return contactosController.actualizar(id)
