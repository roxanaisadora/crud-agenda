import os
from dotenv import load_dotenv
from distutils.util import strtobool


class Environment():

    def __init__(self):
        load_dotenv()

    def general(self):
        return{
            'PORT': int(os.getenv("PORTAPI", 8081)),
            'DEBUG': strtobool(os.getenv('DEBUG', False))
        }

    def db(self):
        return{
            'HOST': os.getenv("HOSTDB", "localhost"),
            'PORT': int(os.getenv("PORTDB", 27017)),
        }
    
    def dbMySQL(self):
        return{
            'HOST': os.getenv("MYSQLHOST", '127.0.0.1'),
            'BD': os.getenv("MYSQLDB", 'agenda'),
            'PORT': int(os.getenv("MYSQLPORT", 3306)),
            'USER': os.getenv("MYSQLUSER", 'root'),
            'PASS': os.getenv("MYSQLPASS", 'salmos19'),
        }
    def swBDD(self):
        return{
            'BDD' : os.getenv('MYSQLMONGO',False)
        }