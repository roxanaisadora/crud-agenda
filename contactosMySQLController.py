from flask import request
from bson.json_util import dumps
from conection import Conection
from bson.objectid import ObjectId

conection = Conection()
campos = ["nombres", "apellidoPaterno",
          "apellidoMaterno", "movil", "fijo", "email"]


class ContactosMySQLController:

    def validarContacto(self, contacto):
        if all(campo in campos for campo in contacto):
            return contacto
        else:
            return None

    def anadir(self):
        contacto = self.validarContacto(request.get_json(force=True))
        if contacto is None:
            return 'Los campos ingresados no son correctos'
        else:
            cursor = conection.conn.cursor()
            sql = "INSERT INTO contactos (nombres,apellidoPaterno,apellidoMaterno,movil,fijo,email)VALUES(%s,%s,%s,%s,%s,%s)"
            cursor.execute(
                sql,(contacto['nombres'],contacto['apellidoPaterno'],contacto['apellidoMaterno'],contacto['movil'],contacto['fijo'],contacto['email'])
            )
            conection.conn.commit()
            return "Contacto anadido"

    def listar(self):
        print("Entro en lsitar")
        cursor = conection.conn.cursor()
        cursor.execute(
            "select * from contactos"
        )
        '''for nombres, movil in cursor.fetchall():
            print("{0} ----> {1}".format(nombres, movil))'''
        
        resultado = cursor.fetchall()
        result = dumps(resultado)
        return result
       

    def mostrar(self, id):
       print("Entro en mostrar")
       cursor = conection.conn.cursor()
       cursor.execute(
           f"select * from contactos where idContactos = {id}"
       )
                
       resultado = cursor.fetchall()
       result = dumps(resultado)
       return result

    def actualizar(self, id):
        contacto = self.validarContacto(request.get_json(force=True))
        if contacto is None:
            return 'Los campos ingresados no son correctos'
        else:
            cursor = conection.conn.cursor()
            sql = "UPDATE contactos SET nombres = %s,apellidoPaterno = %s,apellidoMaterno = %s,movil = %s,fijo = %s,email = %s WHERE idcontactos = %s;"
            cursor.execute(
                sql,(contacto['nombres'],contacto['apellidoPaterno'],contacto['apellidoMaterno'],contacto['movil'],contacto['fijo'],contacto['email'], id)
            )
            conection.conn.commit()
            return "Contacto actualizado"

    def eliminar(self, id):
        cursor = conection.conn.cursor()
        sql = "DELETE FROM contactos WHERE idcontactos = %s;"
        cursor.execute(
                sql,(id)
        )
        conection.conn.commit()
        return "Contacto eliminado"
